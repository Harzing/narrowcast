<?php
$page_title = 'beheer berichten';
#=============================================================================#
#                                                                             #
# voeg content toe aan $content, deze wordt onderaan de pagina weergegeven.   #
# GEEN ECHOS!                                                                 #
#                                                                             #
#=============================================================================#
$content = '';

include ('lib_narrowcasting.php');

$select = ''; 
if(isset($_REQUEST['cat'])) {
   $select = $_REQUEST['cat'];
   }

$content .= menu($db, $select);

if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = "undefine";
} else {
    $actionswitch = $_REQUEST['action'];
}

switch ($_REQUEST['action']) {

    case 'new':
        #$content .= '<h2>' . $actionswitch . '</h2>';
        $content .= message($db);
    break;

    case 'save':
        save_message($db, $_REQUEST);
        header('location: ' . $_SERVER['PHP_SELF'].'?action=edit&message_id='.$returned_id);
        header('location: ' . $_SERVER['PHP_SELF']);
        exit;
    break;

    case 'edit':
        #$content .= '<h2>' . $actionswitch . '</h2>';
        $content .= message($db, 'edit', $_REQUEST['message_id']);
    break;

    case 'save_edit':
        //print_r($_REQUEST);
        $returned_id = save_message($db, $_REQUEST);
        if (!empty($returned_id)) {
            header('location: ' . $_SERVER['PHP_SELF'].'?action=edit&message_id='.$returned_id);
            exit;
        } else {
          $content .= print_error('wijzigen mislukt');
        }

    break;

    case 'single_message':
        if ($_REQUEST['message_id'] > 0) {
            $delmessage = get_message($db, $_REQUEST['message_id']);
            $content .= '<p></p>' . show_message($delmessage);
            $deletemessage  = 'LET OP: op verwijderen drukken werkt onmiddelijk en is niet ongedaan te maken!<p>' . "\n"; 
            $deletemessage .= '<form action="' . $_SERVER['PHP_SELF'] . '" name="bericht" method="post">' . "\n";
            $deletemessage .= '<input type="hidden" name="message_id" value="' . $_REQUEST['message_id'] . '">' . "\n";
            $deletemessage .= '<input type="hidden" name="action" value="delete">' . "\n";
            $deletemessage .= '<input type="submit" name="delete" value="verwijder">' . "\n";
            $deletemessage .= '</form>' . "\n";
            $content .= $deletemessage;
        } else {
            $content .= 'no message selected.';
        }
    break;

    case 'delete':
        if ($_REQUEST['message_id'] > 0) {
            $resultaat = delete_message($db, $_REQUEST['message_id']);
            $content .= '<p>bericht is ' . $resultaat . ' verwijderd.</p>';
        }
    break;

    default:
    $messagelist = get_messagelist($db, $select);
    $content .= show_messagelist($messagelist);
}

//$content .= '<div style="text-align:center; font-size: x-small;">' . $todo . '</div>';

include_once ('header.php');
$pcontent = '<h1>' . $page_title . '</h1>';
$pcontent .= $content;
echo $pcontent;
include_once ('footer.php');
?>
