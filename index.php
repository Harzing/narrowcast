<?php
// NOTE: your phpslideshow.php script will work "out of the box" and
// all layout and visual effects are controlled by the template.html file.
// you can fine tune your slideshow by editing some of these settings.

require ('beheer/lib_narrowcasting.php');

// ------------- BEGIN CONFIG SECTION --

$header = 'Narrowcasting';
$title = 'HPDB';
/*$backgroundimage1 = '/files/images/red_part_of_logo.png';
$backgroundimage2 = '/files/images/red_part_of_logo.png';
*/
// $backgroundimage1 = $backgroundimage2 =  '/' . MY_FILE_DIR .'bg_transparent.png';
//$backgroundimage1 = $backgroundimage2 =  '/' . MY_FILE_DIR .'red_part_of_logo.png';
// language text for various areas...
$lang_back = 'back';
$lang_next = 'next';
$lang_of = 'van';
$lang_stop_slideshow = 'stop slideshow';
$lang_start_slideshow = 'start slideshow';
$lang_img_hover = 'click for next image...';
$lang_img_alt = 'slideshow image';

// automated slideshow options
// remember that you need <META_REFRESH> in the <head> section of your html
// AND the <AUTO_SLIDESHOW_LINK> tag in your page.
//$startauto is -1 (off) or 1 (on) for automatic advancing
$startauto = '1';
// $delay is the number of seconds to pause between slides...
$delay = 1;
// ------------- END CONFIG SECTION --

################################################################################
// begin code :: don't make changes below this line...

// initialize some stuff:
$auto_url = "";

### PATH ###
if (file_exists("phpslideshow/pagetemplate.html")) {
    $template = implode("", file('phpslideshow/pagetemplate.html'));
} else {
    exit('<b>TEMPLATE ERROR:</b> Can\'t find the pagetemplate.html file!');
}


// grab the variables we want set for newer php version compatibility
#$totalPages  = isset($_GET['totalPages']) ? strip_tags($_GET['totalPages']) : get_total_messages($db);
$totalPages  = get_total_messages($db);
$currentPage = isset($_GET['currentPage']) ? strip_tags($_GET['currentPage']) : '1';
$auto        = isset($_GET['auto']) ? strip_tags($_GET['auto']) : $startauto;
$getnext     = get_next_message_id($db, $currentPage);
$next        = (isset($getnext)) ? $getnext: '1';
/*$next_link   = '<a href="' . NARROWCASTINGROOT . '?currentPage=' . $next . '&totalPages=' . $totalPages . '">';
$prev_link   = '<a href="' . NARROWCASTINGROOT . '?currentPage=' . ($currentPage-1) . '&totalPages=' . $totalPages . '">';*/
$next_link   = '<a href="' . NARROWCASTINGROOT . '?currentPage=' . $next . '">';
$prev_link   = '<a href="' . NARROWCASTINGROOT . '?currentPage=' . ($currentPage-1) . '">';

$header .= ' berichten, nummer ' . $currentPage . ' ' .$lang_of . ' ' .$totalPages . ' berichten';
# MONZO addition 04.06.2010
//$caption = '<SHOW_DATE> / <SHOW_TIME>';
//$caption = ($currentPage %2 ) ? '<SHOW_DATE><br><SHOW_TIME>': 'Huisartsenpraktijk <br>Den Bommel';
//$caption = ($currentPage %2 ) ? '<img src="files/images/full_logo_with_letters.png" alt="Huisartsenpraktijk Den Bommel" title="Huisartsenpraktijk Den Bommel" width="400px" />': '<br><SHOW_DATE> / <SHOW_TIME>';
$caption = (date("i") %2 ) ? '<img src="files/images/full_logo_with_letters.png" alt="Huisartsenpraktijk Den Bommel" title="Huisartsenpraktijk Den Bommel" width="400px" />': '<SHOW_DATE> / <SHOW_TIME>';

if (isset($backgroundimage1) && isset($backgroundimage1)) {
    $backgroundimage = ($currentPage %2 ) ? $backgroundimage1 : $backgroundimage2;
    $background = 'style="background-image:url(\'' . $backgroundimage. '\');background-repeat:no-repeat;background-position: left;"';
} else {
    $background='';
}

$message = get_message($db, $currentPage);
//print_r($message);

$realdelay = (empty($message[0]->screentime)) ? $delay : $message[0]->screentime;
if ($auto == "1") {
        $auto_url = '&auto=1';
        //$meta_refresh = "<meta http-equiv='refresh' content='".$realdelay;
        //$meta_refresh .= ";url=".NARROWCASTINGROOT."?currentPage=".$next."&totalPages=".$totalPages."'>";
        $meta_refresh = '<meta http-equiv="refresh" content="' . $realdelay;
        $meta_refresh .= ';url=' . NARROWCASTINGROOT . '?currentPage=' . $next . '" />';
        //$auto_slideshow = "<a href='$path?directory=$path_to_images&currentPage=$currentPage'>$lang_stop_slideshow</a>\n";
        $auto_slideshow = '';
}
else {
        $auto_slideshow = "<a href='$path?auto=1&currentPage=$next&totalPages=".$totalPages."'>$lang_start_slideshow</a>\n";
}

$message[0]->fk_category;
$cat_list = list_category($db, $message[0]->fk_category);

$template = str_replace("<META_REFRESH>",$meta_refresh,$template);
$template = str_replace("<SHOW_TITLE>",$title,$template);
$template = str_replace("<WINDOW_TITLE>",$header,$template);
$template = str_replace("<AUTO_SLIDESHOW_LINK>",$auto_slideshow,$template);
$template = str_replace("<CATEGORYLIST>",$cat_list,$template);
$template = str_replace("<MESSAGESUBJECT>",$message[0]->subject,$template);
$template = str_replace("<MESSAGEBODY>",$message[0]->body,$template);
$template = str_replace("<BACK>",$prev_link,$template);
$template = str_replace("<NEXT>",$next_link,$template);
$template = str_replace("<POSITION>","$currentPage $lang_of $totalPages",$template);
$template = str_replace("<IMAGE_TITLE>",$message[0]->subject,$template);
$template = str_replace("<BODYBACKGROUND>",$background,$template);
$template = str_replace("<PRAKTIJKINFO>",$caption,$template);
$template = str_replace("<SHOW_DATE>",date("d-m-Y"), $template);
$template = str_replace("<SHOW_TIME>",date("H:i"), $template);

echo $template;
?>
