<!doctype html public '-//W3C//DTD HTML 4.01//EN' 'http://www.w3.org/TR/html4/strict.dtd'>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta name="description" content="Narrowcasting">
<meta name="robots" content="index, nofollow">
<meta name="author" content="Menno Harzing">
<title><?php echo $page_title = (isset($page_title)) ? $page_title : 'narrowcasting'; ?></title>
<link type="text/css" rel="stylesheet" href="<?php echo NARROWCASTINGROOT;?>jquery/themes/base/jquery.ui.all.css">
<link type="text/css" rel="stylesheet" href="<?php echo NARROWCASTINGROOT;?>beheer/app.css">
<link type="text/css" rel="stylesheet" href="<?php echo NARROWCASTINGROOT;?>beheer/phpslideshow_v2.css">
<script type="text/javascript" src="<?php echo NARROWCASTINGROOT;?>jquery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="<?php echo NARROWCASTINGROOT;?>jquery/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="<?php echo NARROWCASTINGROOT;?>jquery/ui/minified/jquery.ui.widget.min.js"></script>
<script type="text/javascript" src="<?php echo NARROWCASTINGROOT;?>jquery/ui/minified/jquery.ui.datepicker.min.js"></script>
<script type="text/javascript" src="<?php echo NARROWCASTINGROOT;?>jquery/ui/minified/jquery.ui.position.min.js"></script>
<script type="text/javascript" src="<?php echo NARROWCASTINGROOT;?>jquery/ui/minified/jquery.ui.autocomplete.min.js"></script>
<script type="text/javascript" src="<?php echo NARROWCASTINGROOT;?>jquery/ui/i18n/jquery.ui.datepicker-nl.js"></script>
<script type="text/javascript" src="<?php echo NARROWCASTINGROOT;?>jquery/app-jquery.js"></script>
<script type="text/javascript" src="<?php echo NARROWCASTINGROOT;?>ck/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo NARROWCASTINGROOT;?>ck/ckfinder/ckfinder.js"></script>
<script type="text/javascript">
    CKFinder.setupCKEditor( null, '<?php echo NARROWCASTINGROOT?>ck/ckfinder' ) ;
</script>
</head>
<body>
<div class="wrapper">
<div class='content'>
