<?php
require ('pear_logon_narrowcasting.php');
include_once('definitions.php');
require ('php-pear/MDB2.php');
require ('libtable.php');

$db =& MDB2::factory($dsn, $options);
if (PEAR::isError($db)) { die('ERROR MDB2 factory<br>' . $db->getDebugInfo());}
$db->loadModule('Extended');

# 1 = debug aan, 0 is debug uit
$db->setOption('debug', 0);

#=============#
#   general   #
#=============#

function menu($db, $selected_category) {

    $extendviewlink = (isset($_REQUEST['message_id'])) ? '?currentPage='.$_REQUEST['message_id'] : '';

    $buildmenu = '<div style="margin-top:0.3em;">';
    $buildmenu .= ' | berichten: ';
    $buildmenu .= '<a href="' . NARROWCASTINGROOT . 'beheer/">actief</a> ';
    $buildmenu .= '<a href="' . NARROWCASTINGROOT . 'beheer/?cat=inactief">niet</a> ';
    $buildmenu .= '<a href="' . NARROWCASTINGROOT . 'beheer/?cat=alle">totaal</a> ';
    $buildmenu .= ' | categorie: ';
    $buildmenu .= menu_category($db, $selected_category);

    if (empty($_REQUEST['action'])) {
        $buildmenu .= ' ' . count_messages($db, $selected_category);
    }
    $buildmenu .= ' | <a href="' . NARROWCASTINGROOT . 'beheer/?action=new">nieuw</a> ';
    $buildmenu .= '<a href="' . NARROWCASTINGROOT . $extendviewlink . '">nc-view</a> ';
    $buildmenu .= '</div>';
    return $buildmenu;

}

function print_error($message) {
    # eenduidige foutmeldingen.
    $formatted_message = '<div class="warning">' . $message . '</div>';
    return $formatted_message;
}

function print_sql($locatie, $isql) {
    # sql-debugging
    #
    # $locatie = locatie in script, bijv. __FUNCTION__ voor de functie
    # $isql    = input query
    $prnt_sql  = '<pre>'. $locatie .'<br>';
    $prnt_sql .= $isql;
    $prnt_sql .= '</pre>';
    return $prnt_sql;
}

function mdb2_list_result($db, $sql, $num_cols, $iname, $iselected, $head, $mono = 0)
{
    #functie voor het populeren van keuzelijsten
    # MH 22-5-2007 13:44:02
    # $sql      : de query die uitgevoerd moet worden
    # $num_cols : aantal kolommen dat geselecteerd wordt
    # $iname    : de naam die aan de selectielijs wordt meegegeven.
    # $iselected: het veld dat geselecteerd moet worden in de selectielijst
    # $head     : de waarde voor het lege veld die bovenaan de lijst moet verschijnen
    # $mono     : keuze voor grootte van de lijst en het lettertype

    ($mono) ? $return = '<select name="' . $iname . '" SIZE=10 STYLE="font-family : monospace; font-size : 10pt">' . "\n" : $return = '<select name="' . $iname . '">' . "\n";
    ($head) ? $return .= '<option value="' . $head . '"></option>' . "\n" : $return .= '';

    ($num_cols == 1) ? $i = 0 : $i = 1;

    $res =& $db->query($sql);
    if (PEAR::isError($res)) {
        die($res->getDebugInfo());
    }
    while ($row =& $res->fetchRow(MDB2_FETCHMODE_ORDERED)) {
        if($row['0'] == $iselected) {
            $return .= '<option selected value="' . $row['0'] . '">';
        } else {
            $return .= '<option value="' . $row['0'] . '">';
        }
        $return .= stripslashes($row[$i]) . '</option>' . "\n";

    }
    $return .= '</select>' . "\n";
    return $return;
}

function count_messages($db, $selected_category) {

    unset($cntcatmsg);
    $message_count = '';

    $sql = 'select
                count(id) as count
            from
                message m';

    $result = $db->query($sql);
    $cntmsg = $result->fetchOne(0);

    if (PEAR::isError($result)) {
        return 0;
    } else {
        $result->free();
	    if (!empty($selected_category) && (int)$selected_category) {
		$where = 'where m.fk_category = ' .$db->quote($selected_category, "integer");
	    } elseif ($selected_category == 'alle') {
		$where = '';
	    } elseif ($selected_category == 'inactief') {
		$where = 'where m.show_end < \'' . date("Y-m-d H:i:s", time()).'\'';
	    } else {
		$where = 'where m.show_end >=\'' . date("Y-m-d H:i:s", time()).'\'';
	    }
	    $sql = 'select
                count(id) as count
            from
                message m '
	    .$where;

	    #echo print_sql(__FUNCTION__, $query);
       $result = $db->query($sql);
       $cntcatmsg = $result->fetchOne(0);
	       if (PEAR::isError($result)) {
		       return 0;
	       }
            $result->free();
            $message_count .= ($cntcatmsg > 0) ? $cntcatmsg : '0';
            $message_count .= ' van ';
        }
        $message_count .= $cntmsg;
        return $message_count;
}

#=============#
#   message   #
#=============#
function menu_category($db, $selected) {

    $sql = 'select
                id,
                name
            from
                category
            order by name asc, id asc';

    $result = $db->query($sql);
    $cat_lst = $result->fetchAll(MDB2_FETCHMODE_OBJECT);
    $list = '';
    if (PEAR::isError($result)) {
        return 0;
    } else {
        foreach ($cat_lst as $key) {
            $selected_bold = ($key->id == $selected) ? 'bold' : 'normal';
            $list .=  '&nbsp;<a href="./?cat=' . $key->id . '" style="font-weight:' . $selected_bold . ';">' . $key->name . '</a>';
        }
        return $list;
    }
}

function list_category($db, $selected) {

    $sql = 'select
                id,
                name
            from
                category
            order by id asc';

    $result = $db->query($sql);
    $cat_lst = $result->fetchAll(MDB2_FETCHMODE_OBJECT);

    if (PEAR::isError($result)) {
        return 0;
    } else {
        $list = '<ul class="category">';
        foreach ($cat_lst as $key) {
            $selected_bold = ($key->id == $selected) ? 'bold' : 'normal';
            $list .=  '<li class="category cat' . $selected_bold . '">' . $key->name . '</li> '."\n";
        }
        $list .= '</ul>';
        return $list;
    }
}

function get_messagelist($db, $selected_category) {
    # $selected_category = id van category
    if (!empty($selected_category) && (int)$selected_category) {
        $where = 'where m.fk_category = ' .$db->quote($selected_category, "integer");
    } elseif ($selected_category == 'alle') {
        $where = '';
    } elseif ($selected_category == 'inactief') {
        $where = 'where m.show_end < \'' . date("Y-m-d H:i:s", time()).'\'';
    } else {
        $where = 'where m.show_end >=\'' . date("Y-m-d H:i:s", time()).'\'';
    }
    $query = 'select
                    m.id,
                    m.subject,
                    m.editable,
                    m.sortingnumber,
                    m.screentime,
                    UNIX_TIMESTAMP(m.show_start) as show_start,
                    UNIX_TIMESTAMP(m.show_end) as show_end,
                    c.name as category
                from
                    message m
                join category c on  c.id = m.fk_category
                ' . $where .'
                order by m.sortingnumber ASC, m.id ASC';
    #echo print_sql(__FUNCTION__, $query);
    $sql = $db->prepare($query);
    $res = $sql->execute();

    if (PEAR::isError($res)) { die($res->getDebugInfo()); }

    $exp_mess_lst = array();
    $exp_mess_lst = $res->fetchAll(MDB2_FETCHMODE_OBJECT);

    return $exp_mess_lst;
}

function show_messagelist($inputarray) {

    if (count($inputarray) == 0) { return '<p>geen berichten (in deze categorie)</p>';}
    $countscreentime = 0;

    # necessary for determining inactive messages
    $currenttime = strtotime("now");

    $table = new Table('berichtbeheer', 0, 0);
    $table->AddHeader(array('onderwerp', 'categorie', 'te zien<br>vanaf', 'te zien<br>tot', 'bekijk /<br>verwijder', 'rangorde'));

    foreach ($inputarray as $key) {

        #show that a message is inactive
        if ($currenttime > $key->show_end) {
            $activityclass='inactive_message';
        } else {
            $activityclass='active_message';
        }

        $link_id              = '<a href="?action=edit&message_id=' . $key->id . '">' . $key->id . '</a>';
        $link_subject         = '<a href="?action=edit&message_id=' . $key->id . '">' . $key->subject . '</a>';
        $link_singlemessage   = '<a href="?action=single_message&message_id=' . $key->id . '">bekijk</a>';
        //$date_start = date("d-m-Y H:i:s", $key->show_start);
        //$date_end = date("d-m-Y H:i:s", $key->show_end);
        $date_start           = date("d-m-y, H:i", $key->show_start);
        $date_end             = date("d-m-y H:i", $key->show_end);
        $sortingnumberfield   = $key->sortingnumber;
        $table->AddRow(null, array('<span class="' . $activityclass . '">' . $link_subject . ' (' . $link_id . ')</span>', $key->category, $date_start, '<span class="' . $activityclass . '">' . $date_end . '</span>', $link_singlemessage, $sortingnumberfield),  0);
        $countscreentime      = $countscreentime+$key->screentime;
    }
    $table->AddRow(null, array('&nbsp;', 'totale lengte berichten ' . round($countscreentime/60,1) .  ' minuten'),  Array(2,3));

    return $table->Close();
}

function show_message($inputarray) {

    if (count($inputarray) == 0) { return '<p>geen bericht</p>';}

    $key          = $inputarray[0];
    $table        = new Table('relatie', 1, 0);
    $link_id      = '<a href="?action=edit&amp;message_id=' . $key->id . '">' . $key->id . '</a>';
    $link_subject = '<a href="?action=edit&amp;message_id=' . $key->id . '">' . $key->subject . '</a>';
    $edit         = '<a href="?action=edit&amp;message_id=' . $key->id . '">wijzig</a>';
    $date_start   = date("d-m-Y H:i:s", $key->show_start);
    $date_end     = date("d-m-Y H:i:s", $key->show_end);

    $table->AddRow('onderwerp'      , $link_subject . ' (' . $link_id . ') ' . $edit,  0);
    $table->AddRow('inhoud'         , $key->body,  0);
    $table->AddRow('categorie'      , $key->category,  0);
    $table->AddRow('van/tot'        , $date_start . ' / ' .  $date_end,  0);
    $table->AddRow('aanpasbaar'     , $key->editable,  0);
    $table->AddRow('tijd op scherm' , $key->screentime,  0);
    $table->AddRow('rangorde'       , $key->sortingnumber,  0);
    return $table->Close();
}

function message($db, $mode='append', $message_id=0) {
   # maken en bewerken berichten
    $disable_sorting             = '';
    $disable_sorting_background  = '';

    if ($mode == 'edit' && $message_id == 0) {
        exit(print_error(__FUNCTION__ .'<br> Er is een fout opgetreden bij het ophalen van een bericht voor wijzigen.<br> Ga terug en probeer opnieuw.'));
    }

    if ($mode == 'edit') {
        $action = 'save_edit';
        $submit_name = 'submit_save';
        $submit_value= 'edit';
        $sql = "select
                    m.id,
                    m.subject,
                    m.body,
                    m.screentime,
                    UNIX_TIMESTAMP(m.show_start) as show_start,
                    UNIX_TIMESTAMP(m.show_end) as show_end,
                    c.id as cat_id,
                    c.name as category,
                    m.sortingnumber
                from
                    message m
                join category c on  c.id = m.fk_category
                where m.id = $message_id";
        //echo "$sql<br>";
        $r = $db->queryrow($sql,array(), MDB2_FETCHMODE_OBJECT);
        //echo "$r->naam<br>";
        if (PEAR::isError($r)) {
            die(print_error(__FUNCTION__ . '<br>' . $r->getDebugInfo()));
        }
    } elseif ($mode == 'append') {
        $action ='save';
        $submit_name = 'submit_save';
        $submit_value= 'new';
        # nieuwe berichten kunnen niet gelijk van rangorde voorzien worden
        $disable_sorting= 'disabled';
        $disable_sorting_background= 'style="background-color: #CCC;"';
    }

    $message = '<form action="' . $_SERVER['PHP_SELF'] . '" name="bericht" method="post">' . "\n";
    (isset($message_id)) ? $message .= '<input type="hidden" name="message_id" value="' . $message_id . '">' . "\n" : 1;
    $message .= '<input type="hidden" name="action" value="' . $action . '">' . "\n";

    # klaarzetten data voor weergave in tabel
    $date_start   = isset($r->show_start) ? date("d-m-Y H:i:s", $r->show_start): date("d-m-Y H:i:s");
    $date_end     = isset($r->show_end)   ? date("d-m-Y H:i:s", $r->show_end)  : '01-10-2037 00:00:00';
    $time_it      = isset($r->screentime) ? $r->screentime  : 30;
    $set_cat_id   = isset($r->cat_id)     ? $r->cat_id      : '';
    $sql = 'select id, name from category order by name';

    $catsel = mdb2_list_result($db, $sql, '2', 'icategory', $set_cat_id, '', 0);

    # opbouw tabel met bericht
    $table = new Table('message', 1, 0);

    // sanitize naam
    $rp_subject   = isset($r->subject)       ? htmlspecialchars($r->subject)       : '';
    $rp_body      = isset($r->body)          ? htmlspecialchars($r->body)          : '';
    $rp_sortingnr = isset($r->sortingnumber) ? htmlspecialchars($r->sortingnumber) : '';

    #($r->id) ? $table->AddRow('id', "$r->id", 0) : 1;
    $table->AddRow('onderwerp', '<input type="text" size="35" maxlength="40" name="isubject" value="' . $rp_subject . '">' . "\n", 0);
    $table->AddRow('categorie', '' . $catsel . '', 0);
    $table->AddRow('bericht <div style="font-size:9px;"><br /><strong> TIP!</strong><br />shift+enter: nieuwe regel <br>enter: nieuwe paragraaf</div>', '<TEXTAREA class="ckeditor" name="ibody" COLS=40 ROWS=6>' . $rp_body . '</TEXTAREA>', 0);
    $table->AddRow('toon van', '<input id="fromdatepicker" type="text" size="30" maxlength="50" name="ishow_start" value="' . $date_start . '"> <span style="font-size:10px;">(leeg = direct online, formaat datum: dd-mm-yyy hh:mm  )</span>' . "\n", 0);
    $table->AddRow('toon tot', '<input id="datepicker" type="text" size="30" maxlength="50" name="ishow_end" value="' . $date_end . '"> <span style="font-size:10px;">(leeg = plaatsing voor onbepaalde tijd)</span>' . "\n", 0);
    $table->AddRow('rangorde', '<input type="text" size="10" maxlength="3" name="isortingnumber" value="' . $rp_sortingnr . '" '. $disable_sorting .' ' . $disable_sorting_background. '> <span style="font-size:10px;">(leeg = standaard voor categorie)</span>' . "\n", 0);
    $table->AddRow('schermtijd', '<input type="text" size="10" maxlength="10" name="iscreentime" value="' . $time_it . '"> seconden <span style="font-size:10px;">(leeg = standaard voor categorie)</span>' . "\n", 0);
    $table->AddRow('', '<input type="submit" name="' . $submit_name . '" value="' . $submit_value . '">' . "\n", 0);
    $message .= $table->Close();
    $message .= "</form>\n";
    return $message;
}

function nl2br_limit($string, $num){
    # http://www.php.net/manual/en/function.nl2br.php#98391
    $dirty = preg_replace('/\r/', '', $string);
    $dirty = preg_replace('/\n/', '', $string);
    $clean = preg_replace('/\n{4,}/', str_repeat('<br/>', $num), preg_replace('/\r/', '', $dirty));
    return nl2br($clean);
}

function save_message($db, $inputarray) {

    //print_error(__FUNCTION__, print_r($inputarray));
    # fetch the next ID in the sequence or return php null
    $insertid = $db->extended->getBeforeID('message');

    ## sanitize data
    $subject  = nl2br_limit($inputarray['isubject'], 2);
    $body     = nl2br_limit($inputarray['ibody'], 2);

    //$put_id            = $db->quote($insertid, "integer");
    $put_fk_category   = $db->quote($inputarray['icategory'], "integer");
    $put_srtnmbr       = (!empty($inputarray['isortingnumber'])) ? $inputarray['isortingnumber'] : get_default_sortingnumber($db, $inputarray['icategory']);
    $put_srtnmbr       = $db->quote($put_srtnmbr, "integer");
    $put_screentime    = ($inputarray['iscreentime']) ? $db->quote($inputarray['iscreentime'], "integer") : $db->quote('30', "integer");
    $put_subject       = $db->quote($subject, "text");
    $put_body          = $db->quote($body, "text");
    $put_created       = $db->quote(date("Y-m-d H:i:s"), "timestamp");
    $put_show_start    = $db->quote(date("Y-m-d H:i:s", strtotime($inputarray['ishow_start'])), "timestamp");
    $put_show_end      = (!empty($inputarray['ishow_end'])) ? $db->quote(date("Y-m-d H:i:s", strtotime($inputarray['ishow_end'])), "timestamp") : $db->quote('2037-10-01 00:00:00', "timestamp");

    if(empty($inputarray['message_id'])) {
        $sql_cols = "(subject, created, fk_category, show_start, show_end, screentime, body, sortingnumber)";
        $sql_vals = "($put_subject, $put_created, $put_fk_category, $put_show_start, $put_show_end, $put_screentime, $put_body, $put_srtnmbr)";

        $sql = "insert into message
                $sql_cols
                values
                $sql_vals";
        # return $id or fetch the last inserted id via autoincrement
        //$exportid = $db->extended->getAfterID($insertid, 'message');

    } elseif (!empty($inputarray['message_id']) && (int) $inputarray['message_id'] ) {
        $sql = 'update message
                set
                    subject      = '. $put_subject   . ',
                    body         = '. $put_body      . ',
                    show_start   = '. $put_show_start. ',
                    show_end     = '. $put_show_end  . ',
                    screentime   = '. $put_screentime. ',
                    sortingnumber= '. $put_srtnmbr   . ',
                    fk_category  = '. $put_fk_category . '
                where id = ' . $db->quote($inputarray['message_id'], 'integer');

        $exportid = $inputarray['message_id'];
    }

    //echo print_sql(__FUNCTION__, $sql);
    /**/
    $result = $db->query($sql);

    if (PEAR::isError($result)) {
        //die($result->getDebugInfo());
        return 0;
    } else {
        $result->free();
        return $exportid;
    }
    /**/
}

function get_default_sortingnumber($db, $category) {

    $sql = 'select
                sortingnumber_default
            from
                category c
            where
              c.id= ' .$db->quote($category, "integer");

    //echo $sql;
    $result = $db->query($sql);
    $catsort = $result->fetchOne(0);

    if (PEAR::isError($result)) {
        return 0;
    } else {
        $result->free();
        return $catsort;
    }
}

function get_total_messages($db) {

    $currenttime = strtotime("now");
    $sql = 'select
                count(id) as count
            from
                message m
            where
                (' . $currenttime. ' > UNIX_TIMESTAMP(m.show_start) AND ' . $currenttime. ' < UNIX_TIMESTAMP(m.show_end))';

    //echo $sql;
    $result = $db->query($sql);
    //$cntmsg = $result->fetchOne(0);

    if (PEAR::isError($result)) {
        return 0;
    } else {
    	$cntmsg = $result->fetchone(0);
      $result->free();
      return $cntmsg;
    }/**/
}

function get_next_message_id($db, $imsgid) {
    ### get the next message id to show in the narrowcast based on current ranking or the next available ranking

    #determine ranking of given page
    $sql = 'select
               sortingnumber
            from
                message m
            where
                id = '. $db->quote($imsgid, "integer");
    //echo print_sql(__FUNCTION__, $sql);
    $result = $db->query($sql);
    $sortingnumber = $result->fetchOne(0);

    if (PEAR::isError($result)) {
        $result->free();
        $sortingnumber = '0';
        $andsortingnumber = '';
    } else {
        $result->free();
        $andsortingnumber = ' AND m.sortingnumber = '. $db->quote($sortingnumber, "integer");
    }

    #select next page based on ranking or next id
    $sql = 'select
                id
            from
                message m
            where
                (m.id > '. $db->quote($imsgid, "integer") . $andsortingnumber . ')
            AND m.show_end >= now()
            limit 1';

    //echo $sql;
    $result = $db->query($sql);
    $rmsgid = $result->fetchOne(0);
    //echo 'msgid ' . $rmsgid;
    if (PEAR::isError($result) ||$rmsgid == NULL ) {
        $result->free();
        # get the first message from the next category, based on sortingnumber
        $sql = 'select
                    min(id)
                from
                    message m
                where
                    (m.sortingnumber > '. $db->quote($sortingnumber, "integer") . ')
               AND m.show_end >= now()
                    limit 1';
        //echo $sql;

        $result = $db->query($sql);
        $rmsgid = $result->fetchOne(0);
        //echo 'msgid ' . $rmsgid;
        if (PEAR::isError($result)) {
            $result->free();
            return 0;
        } else {
            $result->free();
            return $rmsgid;
        }/**/
    } else {
        $result->free();
        return $rmsgid;
    }/**/
}

function get_message($db, $messagenumber) {

    $query = 'select
                    m.id,
                    m.subject,
                    m.body,
                    m.editable,
                    m.sortingnumber,
                    m.screentime,
                    m.fk_category,
                    UNIX_TIMESTAMP(m.show_start) as show_start,
                    UNIX_TIMESTAMP(m.show_end) as show_end,
                    c.name as category
                from
                    message m
                join category c on  c.id = m.fk_category
                where
                    m.id = ' . $db->quote($messagenumber);

    //echo print_sql(__FUNCTION__, $query);

    $sql = $db->prepare($query);


    $res = $sql->execute();

    if (PEAR::isError($res)) { die($res->getDebugInfo()); }

    $message = $res->fetchAll(MDB2_FETCHMODE_OBJECT);

    return $message;
}

function delete_message($db, $messagenumber) {

    $sql = 'delete
                from
                    message
                where
                    id = ' . $db->quote($messagenumber, "integer") . '
                limit 1';
    //echo $sql;

    $res = $db->query($sql);
    //$message = $res->fetchAll(MDB2_FETCHMODE_OBJECT);

    if (PEAR::isError($res)) {
        die($res->getDebugInfo());
        $message = 'niet';
    } else {
        $message = 'succesvol';
    }

    return $message;
}
?>
