<?php
/*
DATE        WHO     WHAT
==========  ======  ============================================
18-04-2014  Menno   Change username, password and databasename
==========  ======  ============================================
*/
$dsn = array(
    'phptype'  => 'mysql',
    'username' => '<USERNAME>',
    'password' => '<PASSWORD>',
    'hostspec' => 'localhost',
    'database' => '<DATABASE>',
);

$options = array(
    'debug'       => 2,
    'portability' => MDB2_PORTABILITY_ALL,
);

?>
