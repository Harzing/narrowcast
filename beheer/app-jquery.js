/*
 *
 * 26-03-2010 MH
 * Uitwerking jquery-functies voor app
 *
 */
$(function() {
        $("#fromdatepicker").datepicker({
            numberOfMonths: 3,
            showButtonPanel: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/app/include/jquery/themes/ui-lightness/images/calendar.gif",
            buttonImageOnly: true
        });
        $("#datepicker").datepicker({
            numberOfMonths: 3,
            showButtonPanel: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/app/include/jquery/themes/ui-lightness/images/calendar.gif",
            buttonImageOnly: true
        });
        $("#birthdatepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: "yy-mm-dd",
            showOn: "button",
            buttonImage: "/app/include/jquery/themes/ui-lightness/images/calendar.gif",
            buttonImageOnly: true,
            yearRange: "1930:"
        });
        $("#zoek_gebruiker").autocomplete({
            source: "zoek_relatie.php",
            minLength: 2,
                select: function(event, ui) {
                    document.edituser.relatie_id.value = ui.item.id;
                }
        });
        $("#zoek_relatie").autocomplete({
            source: "zoek_rechtspers.php",
            minLength: 3,
                select: function(event, ui) {
                    document.find_rp.rp_relatie_id.value = ui.item.id;
                }
        });
        $("#zoek_dossier").autocomplete({
            source: "zoek_dossier.php",
            minLength: 3,
                select: function(event, ui) {
                    document.koppeldossier.dossier_id.value = ui.item.id;
                }
        });
});
