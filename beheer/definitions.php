<?php

date_default_timezone_set("Europe/Amsterdam");                    # PHP-required setting

# set up path
#define("WEBFOLDER"        , '/home/hpdb/www/');                    # don't forget trailing slash!
#define("WEBFOLDER"        , 'C:/bin/web/Apache24/htdocs/');        # don't forget trailing slash!
define("WEBFOLDER"        , 'C:/Users/Menno/Sites/');             # don't forget trailing slash!
define("NARROWCASTINGROOT", '~menno/nc/');                              # don't forget trailing slash!

# used for getting TT-pages
define("MY_FILE_DIR",'files/ck_uploaded/images/');                            # don't forget trailing slash!
define("TITLE_FOR_LOG", 'Narrowcasting: ');
define("DOWNLOAD_SITE", 'http://teletekst.nos.nl/gif/images/');   # don't forget trailing slash!
define("DOWNLOAD_FOLDER",  'files/teletekst/');                   # don't forget trailing slash!

?>
