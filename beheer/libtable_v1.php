<?php

// Defines Table & TableSort classes


/* Table generator class met generieke CSS classes & id's */

/*

 Constuctor: Table( $class, $rowheader, $colheader );

    Maakt een instance van de Table class aan.

    $class:     String die voor ieder class= en id= attribute wordt geplakt
    $rowheader: boolean die aangeeft of row headers (linker kolom) moeten worden
            afgedrukt
    $colheader: boolean die aangeeft of automatische column headers (bovenste regel)
            moeten worden afgedrukt. De labels worden in dit geval automatisch
            bepaald a.d.h.v. de keys van het array

Method: Html( $ar );

    Produceert o.b.v. een array definitie automatisch een HTML tabel. (functieresultaat
    is een HTML tabel in stringvorm)

    $ar:        1 of 2-dimensionaal array. Afhankelijk van de $rowheader en $colheader
            settings en de table keys worden automatisch headers toegevoegd

Method: AddRow( $label, $row ,$colspan );

    Voegt een row toe aan de HTML tabel. Als $label niet NULL is en $rowheader TRUE worden
    tevens row headers afgedrukt. Optionele controle over colspan. Geen functieresultaat.

    $label      String die het row label bevat. Mag NULL zijn.
    $row        String of Array met de row data
    $colspan    Array met optionele colspan data, Array(0,2) zorgt ervoor dat het
            tweede element uit $row met colspan=2 wordt afgedrukt.

Method: AddHeader( $row ,$colspan );

        Voegt een header row toe aan de HTML tabel. Optionele controle over colspan.

        $row            String of Array met de row data
        $colspan        Array met optionele colspan data, Array(0,2) zorgt ervoor dat het
                        tweede element uit $row met colspan=2 wordt afgedrukt.

Method: HtmlRow( $label, $row ,$colspan );

    Zie AddRow echter het resultaat wordt niet in-memory gehouden maar wordt direct als
    HTML string teruggegeven.


Method: HtmlHeader( $row ,$colspan );

        Zie AddHeader echter het resultaat wordt niet in-memory gehouden maar wordt direct als
        HTML string teruggegeven.


Method: Close( );

    Sluit de in-memory aangemaakte HTML tabel af en geeft het eindresultaat als een string
    terug. (voor gebruik met de AddRow en AddHeader functies).


======= Voorbeelden =======

 Voorbeeld 1:

 Vanuit een simpel array een tabel opbouwen met autom column headers

 $table=new Table('myclass'0,1);
 echo $table->Html(Array(a => 1, b => 2, c => 3, d => 4));

 Voorbeeld 2:

 Met de in-memory functies een tabel opbouwen met handmatig ingestelde
 column headers. Vervolgens het resulaat van Close() via echo naar de
 browser sturen.

 $table=new Table('myclass',1);
 $table->AddHeader(Array( 'a', 'b', 'c', 'd' ));
 $table->AddRow("Regel 1',Array( 1, 2, 3, 4 ));
 echo $table->Close();

 Voorbeeld 3:

 Als voorbeeld 2 maar nu wordt het resultaat per row direct naar de browser
 afgedrukt (zuiniger met geheugen).

 $table=new Table('myclass',1);
 echo $table->HtmlHeader(Array( 'a', 'b', 'c', 'd' ));
 echo $table->HtmlRow("Regel 1',Array( 1, 2, 3, 4 ));
 echo $table->Close();


 Voorbeeld 4:

 Afdrukken tabel met 2 rows en 4 kolommen, Bovendien voorzien van row en
 (autom) column headers

 $table=new Table('myclass',1,1);
 echo $table->HtmlRow('Regel 1',Array(a => 1, b => 2, c => 3, d => 4));
 echo $table->HtmlRow('Regel 2',Array(a => 5, b => 6, c => 7, d => 8));
 echo $table->Close();

*/

class Table {
    var $s = '';
    var $rowcnt = 0;
    var $hdrcnt = 0;
    var $class = 'table';
    var $colheader = 0;
    var $rowheader = 0;
    var $th = 'th';

    function Table($class='table', $rowheader=0, $colheader=0) {

        $this->class=$class;
        $this->colheader=$colheader;
        $this->rowheader=$rowheader;
        $this->s="\n<table class='$class' id='$class'>\n";
    }

    # Headers moeten eigenlijk ook genummerd worden...

    function AddHeader($row,$colspan=NULL,$suppressheader=1) {

        $colcnt=0;
                $this->s.="  <tr class='".$this->class."_row_top' ".
               "id='".$this->class."_row_h".$this->hdrcnt."'>\n";
        if ($this->rowheader) {
            $this->s.="    <".$this->th." class='".$this->class."_colh' ".
                  "id='".$this->class."_h".$this->hdrcnt."h0'>&nbsp;</".$this->th.">\n";
        }
        foreach ($row as $key => $value) {
            $span=(is_array($colspan) && $colspan[$colcnt] ? "colspan=$colspan[$colcnt] " : '');
            $this->s.="    <".$this->th." class='".$this->class."_col${colcnt}' $span".
                  "id='".$this->class."_h".$this->hdrcnt."c${colcnt}'>$value</".$this->th.">\n";
            $colcnt++;
        }
        $this->s.="  </tr>\n";
        $this->hdrcnt++;
    }

    function AddRow($label,$row,$colspan=NULL) {

        if ($this->colheader == 1 && $this->rowcnt == 0 && is_array($row)) {
            $this->AddHeader(array_keys($row),NULL,0);
        }
                $colcnt=0;
                $this->s.="  <tr class='".$this->class."_row_".($this->rowcnt % 2 ? 'even' : 'odd')."' ".
              "id='".$this->class."_row_r".$this->rowcnt."'>\n";
        if (isset($label) && $this->rowheader) {
            $span=(is_array($colspan) && $colspan[$colcnt] ? "colspan=$colspan[$colcnt] " : '');
            $this->s.="    <".$this->th." class='".$this->class."_colh' $span".
                  "id='".$this->class."_r".$this->rowcnt."h0'>$label</".$this->th.">\n";
            $colcnt++;
        }
        if (is_array($row)) {
                    foreach ($row as $key => $value) {
                $span=(is_array($colspan) && $colspan[$colcnt] ? "colspan=$colspan[$colcnt] " : '');
                            $this->s.="    <td class='".$this->class."_col${colcnt}' $span".
                                      "id='".$this->class."_r".$this->rowcnt."c${colcnt}'>$value</td>\n";
                            $colcnt++;
                    }
        } else {
            $this->s.="    <td class='".$this->class."_col0' ".
                                  "id='".$this->class."_r".$this->rowcnt."c0'>$row</td>\n";
        }
        $this->s.="  </tr>\n";
        $this->rowcnt++;
    }

        function HtmlHeader($row,$colspan=NULL) {

                $this->AddHeader($row,$colspan);
                $s=$this->s;
                $this->s='';
                return $s;
        }

    function HtmlRow($label,$row,$colspan=NULL) {

        $this->AddRow($label,$row,$colspan);
        $s=$this->s;
        $this->s='';
        return $s;
    }

    function Close() {

        $s=$this->s;
        $this->s="\n<table class='".$this->class."'>\n";
        return $s . "</table>\n";
    }

    function Html($table=NULL) {

        if (is_array($table)) {
                    foreach ($table as $key => $value) {
                            $this->AddRow($this->rowheader ? $key : NULL, $value);
                    }
        }
        return $this->Close();
    }

    function TH($new=NULL) {

        if (isset($new)) {
            $this->th=$new;
        }
        return $this->th;
    }
}


// TODO add array sort

class TableSorter {
        var $id = 'sort';
        var $headers = NULL; // Field => Label (num fields are ignored)
    var $imgup = "/templates/images/sortup.gif";
        var $imgdown = "/templates/images/sortdown.gif";
    var $default = '';
    var $forceupper = 0;
    var $forcelower = 0;
    var $map = Array();

        function TableSorter($id, $default='', $headers=NULL ) {

                $this->id=$id;
        $this->default=$default;
        if (isset($headers)) $this->SetColumns($headers);
        }

    function Sort() {

        return implode(',',$this->map);
    }

    function Id() {
        return $this->id;
    }

    function SetImages($imgdown,$imgup) {

        $this->imgup=$imgup;
                $this->imgdown=$imgdown;
    }

    function SetColumnsAssoc($headers=Array()) {

        $dup=Array();
        $this->map=Array();
        $this->headers=$headers;
        $sort=explode(',',$_REQUEST[$this->id]);
                foreach ($sort as $value) {
                        $avalue=preg_replace('/^-/','',$value);
                        if (!$dup[$avalue]) {
                                array_push($this->map,$this->headers[$avalue] ? $value : $this->default);
                                $dup[$avalue]=1;
                        }
                }
    }

    function SetColumns($headers=Array()) {

        $new=Array();
        foreach ($headers as $value) {
            $new[$value]=$value;
        }
        $this->SetColumnsAssoc($new);
    }

    function Sql() {

        if (!isset($this->headers)) {
            echo "<p>ERROR: Should call SetColums or SetColumnsAssoc() before Sql()</p>\n";
        }
        $r=Array();
        $sort=explode(',',$this->Sort());
        foreach ($sort as $value) {
            if ($this->forcelower) $value=strtolower($value);
                        if ($this->forceupper) $value=strtoupper($value);
            if ($value[0] == '-') {
                array_push($r,substr($value,1)." DESC");
            } else {
                        array_push($r,$value);
            }
        }
        return " ORDER BY ".implode(', ',$r);
    }

    function ForceLower() {

        $this->forcelower=1;
        $this->forceupper=0;

    }

        function ForceUpper() {

                $this->forceupper=1;
        $this->forcelower=0;
        }


    function Columns($preserve=Array()) {

        $p=Array();
        $r=Array();
        $prefix='';
        $rprefix='-';
        $image=$this->imgdown;
        $sort=explode(',',$this->Sort());
        if ($sort[0][0] == '-') {
            $prefix='-';
            $rprefix='';
            $image=$this->imgup;
            $sort[0]=substr($sort[0],1);
        }
        $usort=implode(',',$sort);
        foreach ($preserve as $key => $value) {
            array_push($p,urlencode($key).'='.urlencode($value));
        }

        $url="$GLOBALS[PHP_SELF]?".implode('&',$p).($p ? '&' : '');
        foreach ($this->headers as $field => $label) {
            if (is_numeric($field)) {
                array_push($r,$label);
            } else {
                if ($field == $sort[0]) {
                    array_push($r,sprintf(
                            "<a href='%s%s=%s%s'><img src='%s' border=0> </a>".
                            "<a href='%s%s=%s%s'>%s</a>",
                            $url,$this->id,$rprefix,$usort,$image,
                            $url,$this->id,$rprefix,$usort,$label));
                } else {
                    array_push($r,sprintf("<a href='%s%s=%s'>%s</a>",
                                $url,$this->id,"$field,$prefix$usort",$label));
                }
            }
        }
        return($r);
    }

}


class TablePager {
    var $s = '';
    var $curpage = 0;
    var $itemcnt =0;
    var $pagesize=10;

    function TablePager($pagesize=NULL,$itemcnt=NULL,$page=NULL) {

        $this->PageSize($pagesize);
        $this->ItemCnt($itemcnt);
        $this->Page($page);
    }

    function PageSize($new=NULL) {

        if (isset($new) && $new >= 0) {
            $this->pagesize=$new;
        }
        return $this->pagesize;
    }

    function PageCnt() {

        return ceil($this->ItemCnt() / $this->PageSize());

    }

    function ItemCnt($new=NULL) {

        if (isset($new) && $new >= 0) {
                $this->itemcnt=$new;
        }
        return $this->itemcnt;

    }

    function Page($new=NULL) {

        if (isset($new) && $new >= 0 && $new <= $this->PageCnt()) {
            $this->curpage=$new;
        }
        return $this->curpage;
    }

    function Next() {

        if ($this->Page() < $this->PageCnt() -1) {
            return $this->Page($this->Page() + 1);
        }
        return NULL;
    }

    function Previous() {
        if ($this->Page() > 0) {
            return $this->Page($this->Page() - 1);
        }
        return NULL;
    }

    function Prev() {

        $this->Previous();
    }

    function First() {

        return $this->Page() == 0;
    }

    function Last() {

        return $this->Page() == $this->PageCnt() - 1;
    }

    function Pages($template='%p') {

        $r=Array();
        $t=$this->PageCnt();
        $ps=$this->PageSize();
        $ic=$this->ItemCnt();
        for ($p=0; $p < $this->PageCnt(); $p++) {
            $s=$template;
            $l=($p < $t - 1 ? ($p + 1) * $ps : $ic) - 1;
            $s=str_replace('%p',$p,$s); // Page number
            $s=str_replace('%P',$p + 1,$s); // Page number + 1
            $s=str_replace('%t',$t,$s); // Number of pages
            $s=str_replace('%f',$p * $ps,$s); // First item on page
            $s=str_replace('%F',$p * $ps + 1,$s); // First item on page + 1
            $s=str_replace('%l',$l,$s); // Last item on page
            $s=str_replace('%L',$l + 1,$s); // Last item on page + 1
            array_push($r,$s);
        }
        return $r;
    }

    function Sql() {

        return sprintf("LIMIT %d OFFSET %d",
            $this->PageSize(),$this->Page() * $this->PageSize());
    }
}
?>
