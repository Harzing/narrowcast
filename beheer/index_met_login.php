<?php
$page_title = 'beheer berichten';
$content = '';
include_once('definitions.php');

# security
include("/data/www/loginsystem/include/session.php");

#=============================================================================#
# 28-02-2010 12:53 MH:                                                        #
#                                                                             #
# voeg content toe aan $content, deze wordt onderaan de pagina weergegeven.   #
# GEEN ECHOS!                                                                 #
#                                                                             #
#=============================================================================#

if($session->logged_in)
{
   //$content .= "Welcome <b>$session->username</b>, you are logged in. <br><br>"
   /*$content .= "[<a href=\"" . LOGINSYSTEM_WEBFOLDER . "userinfo.php?user=$session->username\">My Account</a>] &nbsp;&nbsp;"
       ."[<a href=\"" . LOGINSYSTEM_WEBFOLDER . "useredit.php\">Edit Account</a>] &nbsp;&nbsp;";
   if($session->isAdmin())
   {
      $content .= "[<a href=\"" . LOGINSYSTEM_WEBFOLDER . "admin/admin.php\">Admin Center</a>] &nbsp;&nbsp;";
   }
   $content .= "[<a href=\"" . LOGINSYSTEM_WEBFOLDER . "process.php\">Logout</a>]";
   $content .= "<p></p>";*/

    include ('lib_narrowcasting.php');
    $content .= menu();

    if (!isset($_REQUEST['action'])) {
        $_REQUEST['action'] = "undefine";
    } else {
        $actionswitch = $_REQUEST['action'];
    }

    switch ($_REQUEST['action']) {

        case 'new':
            $content .= '<h2>' . $actionswitch . '</h2>';
            $content .= message($db);
        break;

        case 'save':
            $content .= save_message($db, $_REQUEST);
        break;

        case 'edit':
            $content .= '<h2>' . $actionswitch . '</h2>';
            $content .= message($db, 'edit', $_REQUEST['message_id']);
        break;

        case 'save_edit':
            print_r($_REQUEST);
        break;

        default:
        $messagelist = get_messagelist($db);
        $content .= show_messagelist($messagelist);
    }

    $content .= '<div style="text-align:center; font-size: x-small;">' . $todo . '</div>';
}
else
{

    #$content .= '<h1>Login</h1>';
    /**
     * User not logged in, display the login form.
     * If user has already tried to login, but errors were
     * found, display the total number of errors.
     * If errors occurred, they will be displayed.
     */
    if($form->num_errors > 0){
       $content .= "<font size=\"2\" color=\"#ff0000\">" . $form->num_errors . " error(s) found</font>";
    }
    $content .= '<form action="' . LOGINSYSTEM_WEBFOLDER . 'process.php" method="POST">
    <table align="center" border="0" cellspacing="0" cellpadding="3">
    <tr>
        <td>Username:</td>
        <td><input type="text" name="user" maxlength="30" value="' . $form->value("user") . '"></td>
        <td>' . $form->error("user") . '</td>
    </tr>
    <tr>
        <td>Password:</td>
        <td><input type="password" name="pass" maxlength="30" value="'. $form->value("pass") .'"></td>
        <td>'. $form->error("pass") . '</td>
    </tr>
    <tr>
        <td colspan="2" align="left"><input type="checkbox" name="remember" ';
        if($form->value("remember") != ""){ $content .= "checked"; }
        $content .= '>';
        $content .= '<font size="2">Remember me next time &nbsp;&nbsp;&nbsp;&nbsp;
        <input type="hidden" name="sublogin" value="1">
        <input type="submit" value="Login"></td>
    </tr>
    <tr>
        <td colspan="2" align="left"><br><font size="2">[<a href="forgotpass.php">Forgot Password?</a>]</font></td>
        <td align="right"></td>
    </tr>
    <tr>
        <td colspan="2" align="left"><br>Not registered? <a href="register.php">Sign-Up!</a></td>
    </tr>
    </table>
    </form>';
}

/**
 * Just a little page footer, tells how many registered members
 * there are, how many users currently logged in and viewing site,
 * and how many guests viewing site. Active users are displayed,
 * with link to their user information.
 */
#$content .= "<b>Member Total:</b> ".$database->getNumMembers()."<br>";
#$content .= "There are $database->num_active_users registered members and ";
#$content .= "$database->num_active_guests guests viewing the site.<br><br>";

if($session->logged_in)
{
   //$content .= "Welcome <b>$session->username</b>, you are logged in. <br><br>"
   $content .= "<p>[<a href=\"" . LOGINSYSTEM_WEBFOLDER . "userinfo.php?user=$session->username\">My Account</a>] &nbsp;&nbsp;"
       ."[<a href=\"" . LOGINSYSTEM_WEBFOLDER . "useredit.php\">Edit Account</a>] &nbsp;&nbsp;";
   if($session->isAdmin())
   {
      $content .= "[<a href=\"" . LOGINSYSTEM_WEBFOLDER . "admin/admin.php\">Admin Center</a>] &nbsp;&nbsp;";
   }
   $content .= "[<a href=\"" . LOGINSYSTEM_WEBFOLDER . "process.php\">Logout</a>]</p>";
}

//include("/data/www/loginsystem/include/view_active.php");
include_once ('header.php');
$pcontent = '<h1>' . $page_title . '</h1>';
$pcontent .= $content;
echo $pcontent;
include_once ('footer.php');
?>

